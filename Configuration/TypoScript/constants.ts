
plugin.tx_teufelsextlanguagedetection_teufelsextlanguagedetectionhttpacceptlanguage {
    view {
        # cat=plugin.tx_teufelsextlanguagedetection_teufelsextlanguagedetectionhttpacceptlanguage/file; type=string; label=Path to template root (FE)
        templateRootPath = EXT:teufels_ext_language_detection/Resources/Private/Templates/
        # cat=plugin.tx_teufelsextlanguagedetection_teufelsextlanguagedetectionhttpacceptlanguage/file; type=string; label=Path to template partials (FE)
        partialRootPath = EXT:teufels_ext_language_detection/Resources/Private/Partials/
        # cat=plugin.tx_teufelsextlanguagedetection_teufelsextlanguagedetectionhttpacceptlanguage/file; type=string; label=Path to template layouts (FE)
        layoutRootPath = EXT:teufels_ext_language_detection/Resources/Private/Layouts/
    }
    persistence {
        # cat=plugin.tx_teufelsextlanguagedetection_teufelsextlanguagedetectionhttpacceptlanguage//a; type=string; label=Default storage PID
        storagePid =
    }
}

plugin.tx_teufelsextlanguagedetection_teufelsextlanguagedetectioniptonation {
    view {
        # cat=plugin.tx_teufelsextlanguagedetection_teufelsextlanguagedetectioniptonation/file; type=string; label=Path to template root (FE)
        templateRootPath = EXT:teufels_ext_language_detection/Resources/Private/Templates/
        # cat=plugin.tx_teufelsextlanguagedetection_teufelsextlanguagedetectioniptonation/file; type=string; label=Path to template partials (FE)
        partialRootPath = EXT:teufels_ext_language_detection/Resources/Private/Partials/
        # cat=plugin.tx_teufelsextlanguagedetection_teufelsextlanguagedetectioniptonation/file; type=string; label=Path to template layouts (FE)
        layoutRootPath = EXT:teufels_ext_language_detection/Resources/Private/Layouts/
    }
    persistence {
        # cat=plugin.tx_teufelsextlanguagedetection_teufelsextlanguagedetectioniptonation//a; type=string; label=Default storage PID
        storagePid =
    }
}

plugin.tx_teufelsextlanguagedetection_teufelsextlanguagedetectiongeolite {
    view {
        # cat=plugin.tx_teufelsextlanguagedetection_teufelsextlanguagedetectiongeolite/file; type=string; label=Path to template root (FE)
        templateRootPath = EXT:teufels_ext_language_detection/Resources/Private/Templates/
        # cat=plugin.tx_teufelsextlanguagedetection_teufelsextlanguagedetectiongeolite/file; type=string; label=Path to template partials (FE)
        partialRootPath = EXT:teufels_ext_language_detection/Resources/Private/Partials/
        # cat=plugin.tx_teufelsextlanguagedetection_teufelsextlanguagedetectiongeolite/file; type=string; label=Path to template layouts (FE)
        layoutRootPath = EXT:teufels_ext_language_detection/Resources/Private/Layouts/
    }
    persistence {
        # cat=plugin.tx_teufelsextlanguagedetection_teufelsextlanguagedetectiongeolite//a; type=string; label=Default storage PID
        storagePid =
    }
}

## EXTENSION BUILDER DEFAULTS END TOKEN - Everything BEFORE this line is overwritten with the defaults of the extension builder
plugin.tx_teufelsextlanguagedetection_teufelsextlanguagedetectiongeolite {
    settings {
        domain {
            isSingleTree = 1
        }
        bDynamicRedirect =
    }
}