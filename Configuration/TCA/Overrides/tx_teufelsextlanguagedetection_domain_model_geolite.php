<?php

if (!defined('TYPO3_MODE')) {
    die ('Access denied.');
}

$sModel = 'tx_teufelsextlanguagedetection_domain_model_geolite';

// add hive icon
$GLOBALS['TCA'][$sModel]['ctrl']['iconfile'] = 'EXT:hive_cpt_brand/Resources/Public/Icons/PNG/hive_16x16.png';

// labels in backend
$GLOBALS['TCA'][$sModel]['ctrl']['label'] = 'sys_lang_uid';
$GLOBALS['TCA'][$sModel]['ctrl']['label_alt'] = 'title';
$GLOBALS['TCA'][$sModel]['ctrl']['label_alt_force'] =  1;

// sorting in backend
$GLOBALS['TCA'][$sModel]['ctrl']['default_sortby'] = 'ORDER BY sys_lang_uid DESC';
