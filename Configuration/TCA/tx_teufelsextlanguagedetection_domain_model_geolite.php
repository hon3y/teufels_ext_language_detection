<?php
return [
    'ctrl' => [
        'title' => 'LLL:EXT:teufels_ext_language_detection/Resources/Private/Language/locallang_db.xlf:tx_teufelsextlanguagedetection_domain_model_geolite',
        'label' => 'backend_title',
        'tstamp' => 'tstamp',
        'crdate' => 'crdate',
        'cruser_id' => 'cruser_id',
        'sortby' => 'sorting',
        'versioningWS' => true,
        'languageField' => 'sys_language_uid',
        'transOrigPointerField' => 'l10n_parent',
        'transOrigDiffSourceField' => 'l10n_diffsource',
        'delete' => 'deleted',
        'enablecolumns' => [
            'disabled' => 'hidden',
            'starttime' => 'starttime',
            'endtime' => 'endtime',
        ],
        'searchFields' => 'backend_title,title,target,sys_lang_uid,info,label_lang,label_switch,label_dismiss',
        'iconfile' => 'EXT:teufels_ext_language_detection/Resources/Public/Icons/tx_teufelsextlanguagedetection_domain_model_geolite.gif'
    ],
    'interface' => [
        'showRecordFieldList' => 'sys_language_uid, l10n_parent, l10n_diffsource, hidden, backend_title, title, target, sys_lang_uid, info, label_lang, label_switch, label_dismiss',
    ],
    'types' => [
        '1' => ['showitem' => 'sys_language_uid, l10n_parent, l10n_diffsource, hidden, backend_title, title, target, sys_lang_uid, info, label_lang, label_switch, label_dismiss, --div--;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:tabs.access, starttime, endtime'],
    ],
    'columns' => [
        'sys_language_uid' => [
            'exclude' => true,
            'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.language',
            'config' => [
                'type' => 'select',
                'renderType' => 'selectSingle',
                'special' => 'languages',
                'items' => [
                    [
                        'LLL:EXT:lang/locallang_general.xlf:LGL.allLanguages',
                        -1,
                        'flags-multiple'
                    ]
                ],
                'default' => 0,
            ],
        ],
        'l10n_parent' => [
            'displayCond' => 'FIELD:sys_language_uid:>:0',
            'exclude' => true,
            'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.l18n_parent',
            'config' => [
                'type' => 'select',
                'renderType' => 'selectSingle',
                'items' => [
                    ['', 0],
                ],
                'foreign_table' => 'tx_teufelsextlanguagedetection_domain_model_geolite',
                'foreign_table_where' => 'AND tx_teufelsextlanguagedetection_domain_model_geolite.pid=###CURRENT_PID### AND tx_teufelsextlanguagedetection_domain_model_geolite.sys_language_uid IN (-1,0)',
            ],
        ],
        'l10n_diffsource' => [
            'config' => [
                'type' => 'passthrough',
            ],
        ],
        't3ver_label' => [
            'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.versionLabel',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'max' => 255,
            ],
        ],
        'hidden' => [
            'exclude' => true,
            'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.hidden',
            'config' => [
                'type' => 'check',
                'items' => [
                    '1' => [
                        '0' => 'LLL:EXT:lang/locallang_core.xlf:labels.enabled'
                    ]
                ],
            ],
        ],
        'starttime' => [
            'exclude' => true,
            'l10n_mode' => 'mergeIfNotBlank',
            'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.starttime',
            'config' => [
                'type' => 'input',
                'size' => 13,
                'eval' => 'datetime',
                'default' => 0,
            ],
        ],
        'endtime' => [
            'exclude' => true,
            'l10n_mode' => 'mergeIfNotBlank',
            'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.endtime',
            'config' => [
                'type' => 'input',
                'size' => 13,
                'eval' => 'datetime',
                'default' => 0,
                'range' => [
                    'upper' => mktime(0, 0, 0, 1, 1, 2038)
                ],
            ],
        ],

        'backend_title' => [
            'exclude' => false,
            'label' => 'LLL:EXT:teufels_ext_language_detection/Resources/Private/Language/locallang_db.xlf:tx_teufelsextlanguagedetection_domain_model_geolite.backend_title',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim,required'
            ],
        ],
        'title' => [
            'exclude' => false,
            'label' => 'LLL:EXT:teufels_ext_language_detection/Resources/Private/Language/locallang_db.xlf:tx_teufelsextlanguagedetection_domain_model_geolite.title',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim,required'
            ],
        ],
        'target' => [
            'exclude' => false,
            'label' => 'LLL:EXT:teufels_ext_language_detection/Resources/Private/Language/locallang_db.xlf:tx_teufelsextlanguagedetection_domain_model_geolite.target',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim,required'
            ],
        ],
        'sys_lang_uid' => [
            'exclude' => false,
            'label' => 'LLL:EXT:teufels_ext_language_detection/Resources/Private/Language/locallang_db.xlf:tx_teufelsextlanguagedetection_domain_model_geolite.sys_lang_uid',
            'config' => [
                'type' => 'input',
                'size' => 4,
                'eval' => 'int,required'
            ]
        ],
        'info' => [
            'exclude' => false,
            'label' => 'LLL:EXT:teufels_ext_language_detection/Resources/Private/Language/locallang_db.xlf:tx_teufelsextlanguagedetection_domain_model_geolite.info',
            'config' => [
                'type' => 'text',
                'cols' => 40,
                'rows' => 15,
                'eval' => 'trim,required'
            ]
        ],
        'label_lang' => [
            'exclude' => false,
            'label' => 'LLL:EXT:teufels_ext_language_detection/Resources/Private/Language/locallang_db.xlf:tx_teufelsextlanguagedetection_domain_model_geolite.label_lang',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim,required'
            ],
        ],
        'label_switch' => [
            'exclude' => false,
            'label' => 'LLL:EXT:teufels_ext_language_detection/Resources/Private/Language/locallang_db.xlf:tx_teufelsextlanguagedetection_domain_model_geolite.label_switch',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim,required'
            ],
        ],
        'label_dismiss' => [
            'exclude' => false,
            'label' => 'LLL:EXT:teufels_ext_language_detection/Resources/Private/Language/locallang_db.xlf:tx_teufelsextlanguagedetection_domain_model_geolite.label_dismiss',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim,required'
            ],
        ],
    
    ],
];
## EXTENSION BUILDER DEFAULTS END TOKEN - Everything BEFORE this line is overwritten with the defaults of the extension builder