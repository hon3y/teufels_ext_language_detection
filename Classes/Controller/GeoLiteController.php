<?php
namespace TEUFELS\TeufelsExtLanguageDetection\Controller;

use GeoIp2\Database\Reader;
use TYPO3\CMS\Fluid\Core\ViewHelper\Exception;
//use TYPO3\CMS\Core\Utility\GeneralUtility;
//use TYPO3\CMS\Extbase\Object\ObjectManager;
//use TYPO3\CMS\Extbase\Mvc\Web\Routing\UriBuilder;

/***
 *
 * This file is part of the "teufels_ext_language_detection" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2018
 *
 ***/

/**
 * GeoLiteController
 */
class GeoLiteController extends \TYPO3\CMS\Extbase\Mvc\Controller\ActionController
{
    /**
     * geoLiteRepository
     *
     * @var \TEUFELS\TeufelsExtLanguageDetection\Domain\Repository\GeoLiteRepository
     * @inject
     */
    protected $geoLiteRepository = null;


    /**
     * @var \TYPO3\CMS\Extbase\Mvc\Web\Routing\UriBuilder
     * @inject
     */
    protected $uriBuilder;


    /**
     * @var string
     */
    public $sPathToMmdb = 'typo3conf/ext/teufels_ext_language_detection/Resources/Private/GeoLite/GeoIP/GeoLite2-Country.mmdb';


    /**
     * geoLiteAction
     *
     * @throws \GeoIp2\Exception\AddressNotFoundException
     * @throws \MaxMind\Db\Reader\InvalidDatabaseException
     * @throws \TYPO3\CMS\Extbase\Mvc\Exception\StopActionException
     * @throws \TYPO3\CMS\Extbase\Mvc\Exception\UnsupportedRequestTypeException
     */
    public function geoLiteAction()
    {

        $aSettings = $this->settings;

        // instance uriBuilder
        $uriBuilder = $this->uriBuilder;

        // instance new reader object
        $oReader = new Reader($this->sPathToMmdb);


        // get and set current ip
        $sRequestIP = $_SERVER['REMOTE_ADDR'];
        // if localhost or docker set ip to custom ip from USA
        if($_SERVER['HOSTNAME'] && strpos($_SERVER['SERVER_NAME'], 'localhost') != -1){
            $sRequestIP = '3.0.0.0';
        }


        // current page uid
        $iRequestPageUid = intval($GLOBALS['TSFE']->id);

        // object :: get country from ip
        $oRecord = $oReader->country($sRequestIP);




        // check if valid $_SERVER['REMOTE_ADDR']
        if(!$oRecord){

            throw new Exception('The IP: '.$sRequestIP.' was not found in the GeoLite Database.');

        } else {
            // valid IP (found in database)

            $sRequestCountryIso = strtolower($oRecord->country->isoCode);
            // $sRequestCountryName = $oRecord->country->name;

            if($sRequestCountryIso){
                // get available country iso from system
                $aAvailableCountryIso = $this->geoLiteRepository->findByTitle($sRequestCountryIso)->toArray();
            }


            if($aSettings['bDynamicRedirect']){
                // ToDo: [1] dynamic redirect :: force redirect depending on request country and available languages
                if($aAvailableCountryIso){
                    // ToDo: REDIRECT TO available language

                    // only redirect if url is simple domain
                    if (($_SERVER['REQUEST_URI'] == '/' or $_SERVER['REQUEST_URI'] == '')) {
                        $uri = $uriBuilder->reset()
                            // ->setTargetPageUid($aAvailableCountryIso[0]->getTarget())
                            ->setTargetPageUid(($aSettings['domain']['isSingleTree'] ? $iRequestPageUid : $aAvailableCountryIso[0]->getTarget()))
                            ->setArguments(array('L' => $aAvailableCountryIso[0]->getSysLangUid()))
                            ->build();
                        $this->redirectToUri($uri, 0, 307);
                    }

                } else {
                    // ToDo: REDIRECT TO EN

                    // only redirect if url is simple domain
                    if (($_SERVER['REQUEST_URI'] == '/' or $_SERVER['REQUEST_URI'] == '')) {
                        $aDefaultCountryIso = $this->geoLiteRepository->findByTitle('en')->toArray();

                        if ($aDefaultCountryIso) {
                            $defaultUri = $uriBuilder->reset()
                                ->setTargetPageUid(($aSettings['domain']['isSingleTree'] ? $iRequestPageUid : $aDefaultCountryIso[0]->getTarget()))
                                ->setArguments(array('L' => $aDefaultCountryIso[0]->getSysLangUid()))
                                ->build();
                            $this->redirectToUri($defaultUri, 0, 307);
                        }
                    }
                }

            } else {
                // ToDo: [2] manual redirect :: select country and click btn ## [SESSION - COOKIE]
                // Open clickable dialog in window (overlay)

                // assign to template
//                $this->view->assign('x', $x);
            }
        }


    }
}
