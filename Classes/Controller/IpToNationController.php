<?php
namespace TEUFELS\TeufelsExtLanguageDetection\Controller;

/***************************************************************
 *
 *  Copyright notice
 *
 *  (c) 2017
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/***
 *
 * This file is part of the "teufels_ext_language_detection" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2018
 *
 ***/

/**
 * IpToNationController
 */
class IpToNationController extends \TYPO3\CMS\Extbase\Mvc\Controller\ActionController
{
    /**
     * ipToNationRepository
     *
     * @var \TEUFELS\TeufelsExtLanguageDetection\Domain\Repository\IpToNationRepository
     * @inject
     */
    protected $ipToNationRepository = NULL;

    /**
     * action ipToNation
     *
     * @return void
     */
    public function ipToNationAction()
    {
        $bDebug = 0;
        $bIpToNationActionSwitch = 0;
        /*
         * Actual sys_language_uid
         */

        $iSysLanguageUid = intval($GLOBALS['TSFE']->sys_language_uid);
        $sCountryIso = '';
        $aCountryIso = $this->ipToNationRepository->getCountryIso();
        if (is_array($aCountryIso) && count($aCountryIso) == 1) {
            $sCountryIso = $aCountryIso[0]['country'];
        }
        /*
         * Private Network
         * e.g. with Docker
         */

        if ($sCountryIso == '01') {
            $sCountryIso = 'de';
        }
        /*
         * ipToNation
         */

        $aIpToNation = [];
        if ($sCountryIso != '') {
            /* @var \TEUFELS\TeufelsExtLanguageDetection\Domain\Model\IpToNation[] $oIp2Nations */
            $oIp2Nations = $this->ipToNationRepository->findByTitle($sCountryIso)->toArray();
            if ($oIp2Nations != NULL && $oIp2Nations != '') {
                foreach ($oIp2Nations as $oIp2Nation) {
                    if ($iSysLanguageUid != $oIp2Nation->getSysLangUid()) {
                        $bIpToNationActionSwitch = 1;
                        $aIpToNation[] = $oIp2Nation;
                    }
                }
            }
        }
        if (isset($_COOKIE['IPD']) && $_COOKIE['IPD'] == 0) {
            $bIpToNationActionSwitch = 0;
        }
        $this->view->assign('bIpToNationActionSwitch', $bIpToNationActionSwitch);
        $this->view->assign('aIpToNation', $aIpToNation);
    }
}
