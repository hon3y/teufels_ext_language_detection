<?php
namespace TEUFELS\TeufelsExtLanguageDetection\Domain\Model;

/***************************************************************
 *
 *  Copyright notice
 *
 *  (c) 2017
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/***
 *
 * This file is part of the "teufels_ext_language_detection" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2018
 *
 ***/

/**
 * HttpAcceptLanguage
 */
class HttpAcceptLanguage extends \TYPO3\CMS\Extbase\DomainObject\AbstractEntity
{
    /**
     * backendTitle
     *
     * @var string
     * @validate NotEmpty
     */
    protected $backendTitle = '';

    /**
     * title
     *
     * @var string
     * @validate NotEmpty
     */
    protected $title = '';

    /**
     * target
     *
     * @var string
     * @validate NotEmpty
     */
    protected $target = '';

    /**
     * sysLangUid
     *
     * @var int
     * @validate NotEmpty
     */
    protected $sysLangUid = 0;

    /**
     * info
     *
     * @var string
     * @validate NotEmpty
     */
    protected $info = '';

    /**
     * labelSwitch
     *
     * @var string
     * @validate NotEmpty
     */
    protected $labelSwitch = '';

    /**
     * labelDismiss
     *
     * @var string
     * @validate NotEmpty
     */
    protected $labelDismiss = '';

    /**
     * Returns the backendTitle
     *
     * @return string $backendTitle
     */
    public function getBackendTitle()
    {
        return $this->backendTitle;
    }

    /**
     * Sets the backendTitle
     *
     * @param string $backendTitle
     * @return void
     */
    public function setBackendTitle($backendTitle)
    {
        $this->backendTitle = $backendTitle;
    }

    /**
     * Returns the title
     *
     * @return string $title
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Sets the title
     *
     * @param string $title
     * @return void
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }

    /**
     * Returns the target
     *
     * @return string $target
     */
    public function getTarget()
    {
        return $this->target;
    }

    /**
     * Sets the target
     *
     * @param string $target
     * @return void
     */
    public function setTarget($target)
    {
        $this->target = $target;
    }

    /**
     * Returns the sysLangUid
     *
     * @return int $sysLangUid
     */
    public function getSysLangUid()
    {
        return $this->sysLangUid;
    }

    /**
     * Sets the sysLangUid
     *
     * @param int $sysLangUid
     * @return void
     */
    public function setSysLangUid($sysLangUid)
    {
        $this->sysLangUid = $sysLangUid;
    }

    /**
     * Returns the info
     *
     * @return string $info
     */
    public function getInfo()
    {
        return $this->info;
    }

    /**
     * Sets the info
     *
     * @param string $info
     * @return void
     */
    public function setInfo($info)
    {
        $this->info = $info;
    }

    /**
     * Returns the labelSwitch
     *
     * @return string $labelSwitch
     */
    public function getLabelSwitch()
    {
        return $this->labelSwitch;
    }

    /**
     * Sets the labelSwitch
     *
     * @param string $labelSwitch
     * @return void
     */
    public function setLabelSwitch($labelSwitch)
    {
        $this->labelSwitch = $labelSwitch;
    }

    /**
     * Returns the labelDismiss
     *
     * @return string $labelDismiss
     */
    public function getLabelDismiss()
    {
        return $this->labelDismiss;
    }

    /**
     * Sets the labelDismiss
     *
     * @param string $labelDismiss
     * @return void
     */
    public function setLabelDismiss($labelDismiss)
    {
        $this->labelDismiss = $labelDismiss;
    }
}
