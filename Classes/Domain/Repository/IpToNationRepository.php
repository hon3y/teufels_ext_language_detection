<?php
namespace TEUFELS\TeufelsExtLanguageDetection\Domain\Repository;

/***************************************************************
 *
 *  Copyright notice
 *
 *  (c) 2017
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

//use TYPO3\CMS\Core\Database\ConnectionPool;
//use TYPO3\CMS\Core\Database\Query\QueryBuilder;
//use TYPO3\CMS\Core\Utility\GeneralUtility;
/***
 *
 * This file is part of the "teufels_ext_language_detection" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2018
 *
 ***/

/**
 * The repository for IpToNations
 */
class IpToNationRepository extends \TYPO3\CMS\Extbase\Persistence\Repository
{
    /**
     * @var array
     */
    protected $defaultOrderings = [
        'sorting' => \TYPO3\CMS\Extbase\Persistence\QueryInterface::ORDER_ASCENDING
    ];

    //    $sql = 'SELECT
    //        country
    //    FROM
    //        ip2nation
    //    WHERE
    //        ip < INET_ATON("' . $_SERVER['REMOTE_ADDR'] . '")
    //    ORDER BY
    //        ip DESC
    //    LIMIT 0,1';
    public function getCountryIso()
    {
        //        /** @var QueryBuilder $queryBuilder */
        //        $queryBuilder = GeneralUtility::makeInstance(ConnectionPool::class)
        //            ->getQueryBuilderForTable('ip2nation');
        //        $rows = $queryBuilder
        //            // Select one ore more fields …
        //            ->select('country')
        //            // … from a table …
        //            ->from('ip2nation')
        //            // … that fits into this condition.
        //            ->where(
        //            // This looks a bit strange, but ensures that the condition is coded in the most suitable way for the database.
        //                $queryBuilder->expr()->lt('ip', 'INET_ATON("' . $_SERVER['REMOTE_ADDR'] . '")')
        //            )
        //            // Run the query …
        //            ->execute()
        //            // … and fetch all results as an associative array.
        //            ->fetchAll();
        $row = $GLOBALS['TYPO3_DB']->exec_SELECTgetRows('country', 'ip2nation', 'ip < INET_ATON("' . $_SERVER['REMOTE_ADDR'] . '")', '', 'ip DESC', '0,1');
        return $row;
    }
}
