<?php
defined('TYPO3_MODE') || die('Access denied.');

call_user_func(
    function()
    {

        \TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerPlugin(
            'TEUFELS.TeufelsExtLanguageDetection',
            'Teufelsextlanguagedetectionhttpacceptlanguage',
            'teufels_ext_language_detection :: httpAcceptLanguage'
        );

        \TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerPlugin(
            'TEUFELS.TeufelsExtLanguageDetection',
            'Teufelsextlanguagedetectioniptonation',
            'teufels_ext_language_detection :: '
        );

        \TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerPlugin(
            'TEUFELS.TeufelsExtLanguageDetection',
            'Teufelsextlanguagedetectiongeolite',
            'teufels_ext_language_detection :: GeoLite :: geoLite'
        );

        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addStaticFile('teufels_ext_language_detection', 'Configuration/TypoScript', 'teufels_ext_language_detection');

        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addLLrefForTCAdescr('tx_teufelsextlanguagedetection_domain_model_httpacceptlanguage', 'EXT:teufels_ext_language_detection/Resources/Private/Language/locallang_csh_tx_teufelsextlanguagedetection_domain_model_httpacceptlanguage.xlf');
        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::allowTableOnStandardPages('tx_teufelsextlanguagedetection_domain_model_httpacceptlanguage');

        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addLLrefForTCAdescr('tx_teufelsextlanguagedetection_domain_model_iptonation', 'EXT:teufels_ext_language_detection/Resources/Private/Language/locallang_csh_tx_teufelsextlanguagedetection_domain_model_iptonation.xlf');
        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::allowTableOnStandardPages('tx_teufelsextlanguagedetection_domain_model_iptonation');

        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addLLrefForTCAdescr('tx_teufelsextlanguagedetection_domain_model_geolite', 'EXT:teufels_ext_language_detection/Resources/Private/Language/locallang_csh_tx_teufelsextlanguagedetection_domain_model_geolite.xlf');
        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::allowTableOnStandardPages('tx_teufelsextlanguagedetection_domain_model_geolite');

    }
);
## EXTENSION BUILDER DEFAULTS END TOKEN - Everything BEFORE this line is overwritten with the defaults of the extension builder