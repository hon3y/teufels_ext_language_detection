<?php
defined('TYPO3_MODE') || die('Access denied.');

call_user_func(
    function()
    {

        \TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
            'TEUFELS.TeufelsExtLanguageDetection',
            'Teufelsextlanguagedetectionhttpacceptlanguage',
            [
                'HttpAcceptLanguage' => 'httpAcceptLanguage'
            ],
            // non-cacheable actions
            [
                'HttpAcceptLanguage' => 'httpAcceptLanguage'
            ]
        );

        \TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
            'TEUFELS.TeufelsExtLanguageDetection',
            'Teufelsextlanguagedetectioniptonation',
            [
                'IpToNation' => 'ipToNation'
            ],
            // non-cacheable actions
            [
                'IpToNation' => 'ipToNation'
            ]
        );

        \TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
            'TEUFELS.TeufelsExtLanguageDetection',
            'Teufelsextlanguagedetectiongeolite',
            [
                'GeoLite' => 'geoLite'
            ],
            // non-cacheable actions
            [
                'GeoLite' => 'geoLite'
            ]
        );

    // wizards
    \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPageTSConfig(
        'mod {
            wizards.newContentElement.wizardItems.plugins {
                elements {
                    teufelsextlanguagedetectionhttpacceptlanguage {
                        icon = ' . \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extRelPath('teufels_ext_language_detection') . 'Resources/Public/Icons/user_plugin_teufelsextlanguagedetectionhttpacceptlanguage.svg
                        title = LLL:EXT:teufels_ext_language_detection/Resources/Private/Language/locallang_db.xlf:tx_teufels_ext_language_detection_domain_model_teufelsextlanguagedetectionhttpacceptlanguage
                        description = LLL:EXT:teufels_ext_language_detection/Resources/Private/Language/locallang_db.xlf:tx_teufels_ext_language_detection_domain_model_teufelsextlanguagedetectionhttpacceptlanguage.description
                        tt_content_defValues {
                            CType = list
                            list_type = teufelsextlanguagedetection_teufelsextlanguagedetectionhttpacceptlanguage
                        }
                    }
                    teufelsextlanguagedetectioniptonation {
                        icon = ' . \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extRelPath('teufels_ext_language_detection') . 'Resources/Public/Icons/user_plugin_teufelsextlanguagedetectioniptonation.svg
                        title = LLL:EXT:teufels_ext_language_detection/Resources/Private/Language/locallang_db.xlf:tx_teufels_ext_language_detection_domain_model_teufelsextlanguagedetectioniptonation
                        description = LLL:EXT:teufels_ext_language_detection/Resources/Private/Language/locallang_db.xlf:tx_teufels_ext_language_detection_domain_model_teufelsextlanguagedetectioniptonation.description
                        tt_content_defValues {
                            CType = list
                            list_type = teufelsextlanguagedetection_teufelsextlanguagedetectioniptonation
                        }
                    }
                    teufelsextlanguagedetectiongeolite {
                        icon = ' . \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extRelPath('teufels_ext_language_detection') . 'Resources/Public/Icons/user_plugin_teufelsextlanguagedetectiongeolite.svg
                        title = LLL:EXT:teufels_ext_language_detection/Resources/Private/Language/locallang_db.xlf:tx_teufels_ext_language_detection_domain_model_teufelsextlanguagedetectiongeolite
                        description = LLL:EXT:teufels_ext_language_detection/Resources/Private/Language/locallang_db.xlf:tx_teufels_ext_language_detection_domain_model_teufelsextlanguagedetectiongeolite.description
                        tt_content_defValues {
                            CType = list
                            list_type = teufelsextlanguagedetection_teufelsextlanguagedetectiongeolite
                        }
                    }
                }
                show = *
            }
       }'
    );
    }
);
## EXTENSION BUILDER DEFAULTS END TOKEN - Everything BEFORE this line is overwritten with the defaults of the extension builder