<?php
namespace TEUFELS\TeufelsExtLanguageDetection\Tests\Unit\Controller;

/**
 * Test case.
 */
class GeoLiteControllerTest extends \TYPO3\CMS\Core\Tests\UnitTestCase
{
    /**
     * @var \TEUFELS\TeufelsExtLanguageDetection\Controller\GeoLiteController
     */
    protected $subject = null;

    protected function setUp()
    {
        parent::setUp();
        $this->subject = $this->getMockBuilder(\TEUFELS\TeufelsExtLanguageDetection\Controller\GeoLiteController::class)
            ->setMethods(['redirect', 'forward', 'addFlashMessage'])
            ->disableOriginalConstructor()
            ->getMock();
    }

    protected function tearDown()
    {
        parent::tearDown();
    }

}
