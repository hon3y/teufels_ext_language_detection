<?php
namespace TEUFELS\TeufelsExtLanguageDetection\Tests\Unit\Controller;

/**
 * Test case.
 */
class HttpAcceptLanguageControllerTest extends \TYPO3\CMS\Core\Tests\UnitTestCase
{
    /**
     * @var \TEUFELS\TeufelsExtLanguageDetection\Controller\HttpAcceptLanguageController
     */
    protected $subject = null;

    protected function setUp()
    {
        parent::setUp();
        $this->subject = $this->getMockBuilder(\TEUFELS\TeufelsExtLanguageDetection\Controller\HttpAcceptLanguageController::class)
            ->setMethods(['redirect', 'forward', 'addFlashMessage'])
            ->disableOriginalConstructor()
            ->getMock();
    }

    protected function tearDown()
    {
        parent::tearDown();
    }

}
