<?php
namespace TEUFELS\TeufelsExtLanguageDetection\Tests\Unit\Controller;

/**
 * Test case.
 */
class IpToNationControllerTest extends \TYPO3\CMS\Core\Tests\UnitTestCase
{
    /**
     * @var \TEUFELS\TeufelsExtLanguageDetection\Controller\IpToNationController
     */
    protected $subject = null;

    protected function setUp()
    {
        parent::setUp();
        $this->subject = $this->getMockBuilder(\TEUFELS\TeufelsExtLanguageDetection\Controller\IpToNationController::class)
            ->setMethods(['redirect', 'forward', 'addFlashMessage'])
            ->disableOriginalConstructor()
            ->getMock();
    }

    protected function tearDown()
    {
        parent::tearDown();
    }

}
