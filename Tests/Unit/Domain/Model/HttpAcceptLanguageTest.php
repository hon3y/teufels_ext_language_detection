<?php
namespace TEUFELS\TeufelsExtLanguageDetection\Tests\Unit\Domain\Model;

/**
 * Test case.
 */
class HttpAcceptLanguageTest extends \TYPO3\CMS\Core\Tests\UnitTestCase
{
    /**
     * @var \TEUFELS\TeufelsExtLanguageDetection\Domain\Model\HttpAcceptLanguage
     */
    protected $subject = null;

    protected function setUp()
    {
        parent::setUp();
        $this->subject = new \TEUFELS\TeufelsExtLanguageDetection\Domain\Model\HttpAcceptLanguage();
    }

    protected function tearDown()
    {
        parent::tearDown();
    }

    /**
     * @test
     */
    public function getBackendTitleReturnsInitialValueForString()
    {
        self::assertSame(
            '',
            $this->subject->getBackendTitle()
        );
    }

    /**
     * @test
     */
    public function setBackendTitleForStringSetsBackendTitle()
    {
        $this->subject->setBackendTitle('Conceived at T3CON10');

        self::assertAttributeEquals(
            'Conceived at T3CON10',
            'backendTitle',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getTitleReturnsInitialValueForString()
    {
        self::assertSame(
            '',
            $this->subject->getTitle()
        );
    }

    /**
     * @test
     */
    public function setTitleForStringSetsTitle()
    {
        $this->subject->setTitle('Conceived at T3CON10');

        self::assertAttributeEquals(
            'Conceived at T3CON10',
            'title',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getTargetReturnsInitialValueForString()
    {
        self::assertSame(
            '',
            $this->subject->getTarget()
        );
    }

    /**
     * @test
     */
    public function setTargetForStringSetsTarget()
    {
        $this->subject->setTarget('Conceived at T3CON10');

        self::assertAttributeEquals(
            'Conceived at T3CON10',
            'target',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getSysLangUidReturnsInitialValueForInt()
    {
        self::assertSame(
            0,
            $this->subject->getSysLangUid()
        );
    }

    /**
     * @test
     */
    public function setSysLangUidForIntSetsSysLangUid()
    {
        $this->subject->setSysLangUid(12);

        self::assertAttributeEquals(
            12,
            'sysLangUid',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getInfoReturnsInitialValueForString()
    {
        self::assertSame(
            '',
            $this->subject->getInfo()
        );
    }

    /**
     * @test
     */
    public function setInfoForStringSetsInfo()
    {
        $this->subject->setInfo('Conceived at T3CON10');

        self::assertAttributeEquals(
            'Conceived at T3CON10',
            'info',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getLabelSwitchReturnsInitialValueForString()
    {
        self::assertSame(
            '',
            $this->subject->getLabelSwitch()
        );
    }

    /**
     * @test
     */
    public function setLabelSwitchForStringSetsLabelSwitch()
    {
        $this->subject->setLabelSwitch('Conceived at T3CON10');

        self::assertAttributeEquals(
            'Conceived at T3CON10',
            'labelSwitch',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getLabelDismissReturnsInitialValueForString()
    {
        self::assertSame(
            '',
            $this->subject->getLabelDismiss()
        );
    }

    /**
     * @test
     */
    public function setLabelDismissForStringSetsLabelDismiss()
    {
        $this->subject->setLabelDismiss('Conceived at T3CON10');

        self::assertAttributeEquals(
            'Conceived at T3CON10',
            'labelDismiss',
            $this->subject
        );
    }
}
